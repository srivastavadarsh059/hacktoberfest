#include<bits/stdc++.h>
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define ll long long int
#define ld long double
#define fast_cin() ios_base::sync_with_stdio(false); cin.tie(NULL)
#define inarr(arr,n); for(ll i=0;i<n;i++) cin >> arr[i];
#define outarr(arr,n); for(ll i=0;i<n;i++) cout<<arr[i]<<" ";
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
const ll MOD = 1e9+7; // 998244353
const ll INF = 1000000009;
const ll MAXN = 300050;
using namespace std;
typedef vector<ll> vll;
using namespace std;

ll flag = 0;

ll toNum(string s)
{
    ll n=0;
    ll c=0;
    for(ll i=s.size()-1;i>=0;i--)
    {
        n = n + (s[i]-'0') * pow(10,c++);
    }
    return n;
}
string toString(ll n)
{
    string s = "";
    if(n==0)
    {
        return "00";
    }
    while(n>0)
    {
        s = (char) ((n%10) + '0') + s;
        n/=10;
    }
    if(s.length()==1)
    {
        s = "0" + s;
    }
    return s;
}

string toTime(ll min)
{
    ll h = 0, m=0;
    h = (min/60)%24;
    m = min%60;

    string time = toString(h) + ":" + toString(m);

    return time;
    
}

bool check(string s)
{
    string t = s;
    reverse(all(t));
    if(t==s)
        return true;
    return false;
}

void solve()
{
    ll n,H,M;
    cin>>n>>H>>M;
    vll h(n),m(n);

    vll mnn(n);
    for(ll i=0;i<n;i++)
    {
        cin>>h[i]>>m[i];
        mnn[i] = h[i]*60 + m[i];
    }
    sort(all(mnn));
    ll MNN = H*60 + M;
    MNN%=1440;
    ll ans = 0;
    if(mnn.back()<MNN)
    {
        ans = (1440 + mnn[0] - MNN)%1440;
    }
    else
    {
        for(ll i=0;i<mnn.size();i++)
        {
            if(mnn[i]>=MNN)
            {
                ans = (1440 + mnn[i]-MNN)%1440;
                break;
            }
        }
    }
    cout<<ans/60<<" "<<ans%60<<endl;
}




  


int main()
{
    fast_cin();
    ll t=1;
    cin>>t;

    while(t--)
    {
        solve();
    }  
}
    
    