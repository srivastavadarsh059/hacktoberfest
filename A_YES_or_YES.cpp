#include<bits/stdc++.h>
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define ll long long int
#define ld long double
#define fast_cin() ios_base::sync_with_stdio(false); cin.tie(NULL)
#define inarr(arr,n); for(ll i=0;i<n;i++) cin >> arr[i];
#define outarr(arr,n); for(ll i=0;i<n;i++) cout<<arr[i]<<" ";
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
const ll MOD = 1e9+7; // 998244353
const ll INF = 1000000009;
const ll MAXN = 300050;
using namespace std;
typedef vector<ll> vll;
using namespace std;

ll flag = 0;

ll toNum(string s)
{
    ll n=0;
    ll c=0;
    for(ll i=s.size()-1;i>=0;i--)
    {
        n = n + (s[i]-'0') * pow(10,c++);
    }
    return n;
}
string toString(ll n)
{
    string s = "";
    if(n==0)
    {
        return "0";
    }
    while(n>0)
    {
        s = (char) ((n%10) + '0') + s;
        n/=10;
    }
    return s;
}


void solve()
{
    string s;
    cin>>s;
    if(s.length()!=3)
    {
        cout<<"NO"<<endl;
        return;
    }
    if((s[0]=='y' || s[0]=='Y') && (s[1]=='e' || s[1]=='E') && (s[2]=='S' || s[2]=='s')){
        cout<<"YES"<<endl;
    }
    else

    {
        cout<<"NO"<<endl;
    }

}




  


int main()
{
    fast_cin();
    ll t=1;
    cin>>t;

    while(t--)
    {
        solve();
    }  
}
    
    