#include<stdio.h>
#include<bits/stdc++.h>

using namespace std;

int main()
{
    int t=0;
    cin >>t;

    while(t--)
    {
     int x,y;
     cin>>x>>y;
     
     if((x+y*2)%2!=0)
     {
         cout<<"NO"<<endl;
     }
     else if(x==0 && y%2!=0)
     {
          cout<<"NO"<<endl;
     }
     
     else
     cout<<"YES"<<endl;
    }

    return 0;
}