#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define nlc '\n'
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);

typedef long long ll;

const int maxx = 1e18 + 7;
const string yes = "Yes\n", no = "No\n";

// clang-format on
void setIO(string prob_name = "") {
  string input_fpref = "input";
  string output_fpref = "output";
  string input_fext = ".txt";
  string output_fext = ".txt";
  if (prob_name.length()) {
    input_fpref = output_fpref = prob_name;
    input_fext = ".in";
    output_fext = ".out";
  }
  freopen((input_fpref + input_fext).c_str(), "r", stdin);
  freopen((output_fpref + output_fext).c_str(), "w", stdout);
}
// clang-format off

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

#define readOnly freopen("input.txt", "r", stdin);

namespace ___debug {
    template <typename T> struct is_outputtable { template <typename C> static constexpr decltype(declval<ostream &>() << declval<const C &>(), bool()) test(int64_t) { return true; } template <typename C> static constexpr bool test(...) { return false; } static constexpr bool value = test<T>(int64_t()); };
    template <class T, typename V = decltype(declval<const T &>().begin()), typename S = typename enable_if<!is_outputtable<T>::value, bool>::type> void pr(const T &x);
 
    template <class T, typename V = decltype(declval<ostream &>() << declval<const T &>())> void pr(const T &x) { cout << x; }
    template <class T1, class T2> void pr(const pair<T1, T2> &x);
    template <class Arg, class... Args> void pr(const Arg &first, const Args &...rest) { pr(first); pr(rest...); }
 
    template <class T, bool pretty = true> void prContain(const T &x) { if (pretty) pr("{"); bool fst = 1; for (const auto &a : x) pr(!fst ? pretty ? ", " : " " : "", a), fst = 0; if (pretty) pr("}"); }
 
    template <class T> void pc(const T &x) { prContain<T, false>(x); pr("\n"); }
    template <class T1, class T2> void pr(const pair<T1, T2> &x) { pr("{", x.first, ", ", x.second, "}"); }
    template <class T, typename V, typename S> void pr(const T &x) { prContain(x); }
    void ps() { pr("\n"); }
    template <class Arg> void ps(const Arg &first) { pr(first); ps(); }
    template <class Arg, class... Args> void ps(const Arg &first, const Args &...rest) { pr(first, " "); ps(rest...); }
}
using namespace ___debug;

#define __pn(x) pr(#x, " = ")
#ifdef DEBUG_LOCALE
#define deb(...) pr("\033[1;31m"), __pn((__VA_ARGS__)), ps(__VA_ARGS__), pr("\033[0m"), cout << flush
#else
#define deb(...)
#endif

#define readOnly freopen("input.txt", "r", stdin);
auto cleanRemove = [](vector<int> &vx, int num) {
  return vx.erase(remove(fullLength(vx), num), vx.end());
};

// clang-format on
#pragma endregion

auto safeCeil = [](int a, int b = 2) -> int {
  return ((a == b and a == 1) ? 1 : (a + b - 1) / b);
};

// cout << fixed << setprecision(10) << stuff;

int csrt(int x) {
  int y = max(0LL, (int)sqrt(x) - 5);
  while (y * y < x) y++;
  if (y * y != x) return -1;
  return y;
}

auto toBin = [](int a) -> int {
  string ans;
  if (a == 0) return 1LL;
  while (a) {
    ans = to_string(a & 1) + ans;
    a >>= 1;
  }
  return stoll(ans);
};

struct coord {
  int x, y;
};

const int mod = 1e18 + 7;
int calcFac(int n) {
  int fac = 1;
  for (int i = n; i > 0; i--) fac *= i, fac %= mod;
  return fac;
}

vector<pair<int, int>> bl(9);

// adjust dims - it consumes memory
int btable[1][1];
int lim = 1e5;
set<int> gsa;
/* Used in https://codeforces.com/contest/192/problem/A */
void pps() {
  for (int i = 1; i < lim; ++i) gsa.insert((i * (i + 1)) / 2);
}

// ring

// http://www.usaco.org/index.php?page=viewproblem2&cpid=1038 - Social Distancing
void solve(int tc) {
  int n, m;
  cin >> n >> m;
  vector<pair<int, int>> va(m);
  vector<int> vv;
  for (auto &[i, j] : va) cin >> i >> j, vv.push_back(i), vv.push_back(j);
  sort(fullLength(va)), sort(fullLength(vv));

  auto in_range = [&](int en, int idx) {
    return (va[idx].first - 1 < en and en < va[idx].second + 1);
  };

  auto ok = [&](const int d) -> int {
    int cur = 0, cx = vv[0];
    int prev = cx;
    for (int i = 0; i < n;) {
      while (in_range(cx, cur)) {
        prev = cx;
        ++i;
        if (i == n) break;
        cx += d;
      }
      auto lb = lower_bound(fullLength(vv), cx);
      if (*lb < cx) {
        return false;
      }
      // should be @5(for d = 2), but lies in [4, 8](consider
      // pattern to be [1, 3], [4, 8]), so now it
      // should be at 5, not its lb, i.e., 8
      cur = (lb - vv.begin()) / 2LL;
      cx = max(cx, va[cur].first);
      ++i;
      cx += d;
    }
    return true;
  };

  int start = 1, end = vv[2 * m - 1] - vv[0] + 1;
  while (end - start > 1) {
    int mid = end - (end - start) / 2;
    if (ok(mid)) {
      start = mid;
    } else
      end = mid;
  }
  cout << start << nlc;
}

int32_t main() {
  // clang-format off
  fastIO
  bool ldeb = false;
  string pname;
  #ifdef DEBUG_LOCALE
      readOnly
      ldeb = true;
  #elif (NORM_LOCALE)
      pname = "";
  #elif !(ONLINE_JUDGE) // apparently this flag is _not_ set on USACO OJ
      pname = "socdist";
  #endif

  if (!ldeb) setIO(pname);

  // pps();
  int t = 1;

  // cin >> t;
  int tc = 0;
  // cout << fixed << setprecision(9);
  while (t > tc++)
  solve(tc);
}
