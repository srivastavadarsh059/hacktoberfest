#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define nlc '\n'
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("paint.in", "r", stdin); freopen("paint.out", "w", stdout);
#define readOnly freopen("input.txt", "r", stdin);

const int maxx = 1e18 + 7;
const int cmod = 1e9 + 7;
const string yes = "Yes\n", no = "No\n";

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

auto cleanRemove = [](vector<int> &vx, int num) {
  return vx.erase(remove(fullLength(vx), num), vx.end());
};

// clang-format on
#pragma endregion

auto safeCeil = [](int a, int b) -> int {
  return ((a == b and a == 1) ? 1 : (a + b - 1) / b);
};

struct coord {
  int val, wt, ind;
};

void solve() {
  int a, b, c, d;
  cin >> a >> b >> c >> d;
  if (a > c) {
    swap(a, c), swap(b, d);
  }
  int res;
  // *   *    ^      ^
  if (b > d)
    res = b - a;
  else if (b < c)
    res = b - a + d - c;
  else
    res = d - a;
  cout << res << nlc;
}

int32_t main() {
  // clang-format off
  fastIO
  // #ifdef DEBUG_LOCALE
  //     readOnly
  // #elif !defined (ONLINE_JUDGE)
      fileIO
  // #endif

  int t = 1;

  // cin >> t;
  while (t--)
  solve();

}
