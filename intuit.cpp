#include <bits/stdc++.h>

using namespace std;

#define fileIO freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
int main() {
    fileIO
  int n, m;
  cin >> n;
  map<string, map<string, int>> given;
  for (int i = 0; i < n; ++i) {
    string a, b;
    cin >> a >> b;
    ++given[a][b];
  }
  int ok = 0;
  cin >> m;
  for (int i = 0; i < m; ++i) {
    string a, b;
    cin >> a >> b;
    if(given.find(a) != given.end()) {
        if(given[a].find(b) != given[a].end()){
          if (given[a][b] > 0) --given[a][b], ++ok;
      }
    }
  }
  cout << m - ok << '\n';
  }
