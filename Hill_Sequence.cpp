#include<stdio.h>
#include<bits/stdc++.h>

using namespace std;

int main()
{
    int t=0;
    cin >>t;

    while(t--)
    {
     int n;
     cin>>n;

     unordered_set<int>s;
     vector<int> res;

     int a=0;
     int flag=1;
     int dup=0;

     for(int i=0;i<n;i++)
     {
         cin>>a;
         res.push_back(a);

         if(s.find(a)==s.end())
         s.insert(a);

         else if(flag==1)
         {
            dup=a;
            flag=0;
         }

         else
         {
             flag=-1;
             cout<<"-1"<<endl;
             break;
         } 
     }

     if(flag==-1)
     continue;

    sort(res.begin(),res.end(),greater<int>());

     if (s.size()==n)
    {
      for(int i=0;i<n;i++)
      {
          cout<<res[i]<<" ";
      }
    }
    
    else
      {
          if(res[0]==res[1])
          {
              cout<<"-1";
          }

          else
          {
              flag=1;
              cout<<dup<<" "<<res[0]<<" ";
              for(int i=1;i<res.size();i++)
              {
                  if(res[i]==dup && flag==1)
                  {
                      flag=0;
                      continue;
                  }
                  cout<<res[i]<<" ";
              }
          }
     }
     cout<<endl;
    }
    return 0;
}